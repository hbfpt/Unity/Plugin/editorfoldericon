# EditorFolderIcon

您是否曾经想过突出显示经常使用的项目文件夹？这一简单而丰富多彩的资源可以实现这一功能！

使用  **Rainbow Folders** ，您可以在 Unity 项目浏览器中为任何文件夹设置自定义图标和背景。

只需按住  **Alt 键** ，然后单击任何一个文件夹。接着会出现一个配置对话框，您可以为其分配一个自定义图标和背景，您可以自己指定一个图标或从几十个预设中选择一项！

**功能：**
• 在“项目”窗口中更改任何文件夹的图标和背景。
• 一次更改多个文件夹的图标或背景
• 自动为所有子文件夹应用自定义图标和背景
• 可选的行底纹和项目树轮廓
• 包括 70 多个预制图标
• 支持 Unity Collaborate 叠加
• 支持 Unity 版本控制叠加
• 包含源代码

[Rainbow Folders 2 | 实用工具 工具 | Unity Asset Store](https://assetstore.unity.com/packages/tools/utilities/rainbow-folders-2-143526)

## 如何使用

1. 复制git地址：`https://gitlab.com/hbfpt/Unity/Plugin/editorfoldericon.git`
2. 打开工程，点击窗口->包管理器->加号->添加来自git URL的包

![image.png](https://s2.loli.net/2023/04/08/b1rn9XVG6diJq7v.png)

3. 输入复制的git地址
4. 等待下载完成即可！
